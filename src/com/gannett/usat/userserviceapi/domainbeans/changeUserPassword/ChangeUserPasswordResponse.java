package com.gannett.usat.userserviceapi.domainbeans.changeUserPassword;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.userserviceapi.domainbeans.UserServiceBaseAPIResponse;

public class ChangeUserPasswordResponse extends UserServiceBaseAPIResponse implements Serializable {

	private static final long serialVersionUID = -2995100213253779794L;
	private int errorCode = 0;
	private String message = null;

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.errorCode != 0) {
			containsErrors = true;
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.errorCode != 0) {
			aMessage = this.message;
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorcode) {
		errorCode = errorcode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String messages) {
		message = messages;
	}
}