package com.gannett.usat.userserviceapi.domainbeans;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class ErrorsDeserializer implements JsonDeserializer<Meta> {
	public static final String emptyString = "";

	@Override
	public Meta deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

		Meta errors = new Meta();

		if (json == null || json.isJsonNull() || json.toString().trim().equals("[]")) {
			return errors;
		}

		try {
			// Since the errors json may not always have the same names we just want a generic collection of error objects
			JsonObject jObject = json.getAsJsonObject();
			Set<Map.Entry<String, JsonElement>> elements = jObject.entrySet();

			for (Map.Entry<String, JsonElement> entry : elements) {
				JsonArray valueAsArray = entry.getValue().getAsJsonArray();
				if (valueAsArray.size() > 0) {
					Iterator<JsonElement> itr = valueAsArray.iterator();
					while (itr.hasNext()) {
						JsonElement je2 = itr.next();
						Error e = new Error();
						if (entry.getKey() != null && entry.getKey().trim().length() > 0) {
							e.setName(entry.getKey());
						} else {
							e.setName(ErrorsDeserializer.emptyString);
						}
						e.setValue(je2.getAsString());
						errors.addError(e);
					}
				}
			}
		} catch (Exception e) {
			//
			System.out.println("ErrorsDeserializer() - Unable to parse JSON errors: " + json.toString());
		}

		return errors;
	}

}
