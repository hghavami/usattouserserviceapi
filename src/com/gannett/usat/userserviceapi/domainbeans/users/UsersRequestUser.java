package com.gannett.usat.userserviceapi.domainbeans.users;

import java.io.Serializable;

public class UsersRequestUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5565536352425189016L;
	private String fireflyUserId = null;

	public String getFireflyUserId() {
		return fireflyUserId;
	}

	public void setFireflyUserId(String fireflyUserId) {
		this.fireflyUserId = fireflyUserId;
	}
}
