package com.gannett.usat.userserviceapi.domainbeans.changePassword;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.userserviceapi.domainbeans.UserServiceBaseAPIResponse;

public class ChangePasswordResponse extends UserServiceBaseAPIResponse implements Serializable {

	private static final long serialVersionUID = 1241336168367453850L;
	private ChangePasswordRequestMeta meta = null;
	private ChangePasswordRequestResponse response = null;

	public ChangePasswordRequestMeta getMeta() {
		return meta;
	}

	public void setMeta(ChangePasswordRequestMeta meta) {
		this.meta = meta;
	}

	public ChangePasswordRequestResponse getResponse() {
		return response;
	}

	public void setResponse(ChangePasswordRequestResponse response) {
		this.response = response;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.meta != null) {
			if (!this.meta.getMessage().trim().equals("Success") || this.meta.getStatus() > 0) {
				containsErrors = true;
			}
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.meta != null) {
			if (meta.getMessage().trim().equals("Success") || meta.getStatus() == 0) {
				aMessage = "";  //Success, no message is returned
			} else {
				aMessage = meta.getMessage();				
			}
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getWarningCode() {
		if (response == null || response.getWarningCode() == null) {
			return "";
		} else {
			return response.getWarningCode();			
		}
	}
}