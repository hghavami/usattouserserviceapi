package com.gannett.usat.userserviceapi.domainbeans.changePassword;

import java.io.Serializable;

public class ChangePasswordRequestResponse implements Serializable {

	private static final long serialVersionUID = -2258358989684406376L;
	private String warningCode = null;
	private String warningMessage = null;
	
	public String getWarningCode() {
		return warningCode;
	}
	public void setWarningCode(String warningcode) {
		this.warningCode = warningcode;
	}
	public String getWarningMessage() {
		return warningMessage;
	}
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	
}
