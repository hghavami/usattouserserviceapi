package com.gannett.usat.userserviceapi.domainbeans;

import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ErrorsSerializer implements JsonSerializer<Meta> {

	@Override
	public JsonElement serialize(Meta errors, Type typeOfSource, JsonSerializationContext context) {
		JsonElement errorsElement = null;

		Collection<Error> errorCollection = errors.getErrors();

		if (errorCollection.size() > 0) {
			JsonObject jObject = new JsonObject();
			for (Error e : errorCollection) {
				JsonArray eArray = new JsonArray();
				JsonElement anError = new JsonPrimitive(e.getValue());
				eArray.add(anError);
				jObject.add(e.getName(), eArray);
			}
			errorsElement = jObject;
		} else {
			errorsElement = new JsonObject();
		}
		return errorsElement;
	}

}
