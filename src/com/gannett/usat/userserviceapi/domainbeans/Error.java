package com.gannett.usat.userserviceapi.domainbeans;

import java.io.Serializable;

public class Error implements Serializable {

	private static final long serialVersionUID = -7438329361219989304L;
	private String name = null;
	private String value = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
