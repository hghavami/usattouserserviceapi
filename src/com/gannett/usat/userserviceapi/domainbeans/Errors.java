package com.gannett.usat.userserviceapi.domainbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Errors implements Serializable {

	private static final long serialVersionUID = 3674060510200103209L;
	private Collection<Error> errors = null;

	public Errors() {
		super();
		this.errors = new ArrayList<Error>();
	}

	public Collection<Error> getErrors() {
		return errors;
	}

	public void setErrors(Collection<Error> errors) {
		this.errors = errors;
	}

	public void addError(Error err) {
		this.errors.add(err);
	}

}
