package com.gannett.usat.userserviceapi.domainbeans.getUser;

import java.io.Serializable;

public class GetUserRequestUsers implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8139895909422929866L;
	private String fireflyUserId = null;
	
	public String getFireflyUserId() {
		return fireflyUserId;
	}
	public void setFireflyUserId(String fileflyUserId) {
		this.fireflyUserId = fileflyUserId;
	}
	
}
