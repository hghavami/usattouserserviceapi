package com.gannett.usat.userserviceapi.domainbeans.getUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.userserviceapi.domainbeans.UserServiceBaseAPIResponse;

public class GetUserResponse extends UserServiceBaseAPIResponse implements Serializable {

	private static final long serialVersionUID = -6301193474470686181L;
	private int errorCode = 0;
	private String message = null;
	private GetUserRequestUsers[] users = null;

	public GetUserRequestUsers[] getUsers() {
		return users;
	}

	public void setUsers(GetUserRequestUsers[] users) {
		this.users = users;
	}

	@Override
	public boolean containsErrors() {
		boolean containsErrors = false;
		if (this.errorCode != 0) {
			containsErrors = true;
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				containsErrors = true;
			}
		}
		return containsErrors;
	}

	@Override
	public Collection<String> getErrorMessages() {
		Collection<String> messages = new ArrayList<String>();
		String aMessage = "";
		if (this.errorCode != 0) {
			aMessage = this.getMessage();				
			messages.add(aMessage);
		} else {
			if (this.getRawResponse() != null && this.getRawResponse().length() > 0) {
				messages.add(this.getRawResponse());
			}
		}
		return messages;
	}

	public String getFireflyUserId() {
		if (this.users != null && this.users[0] != null && this.users[0].getFireflyUserId() != null) {
			return this.users[0].getFireflyUserId();
		} else {
			return "";			
		}
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorcode) {
		errorCode = errorcode;
	}

	public void setMessage(String messages) {
		message = messages;
	}

	public String getMessage() {
		return message;
	}
}