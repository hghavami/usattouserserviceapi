package com.gannett.usat.userserviceapi.domainbeans;

/**
 * This class represents the JSON menu_options entity which is used in Ruby ICON clients. It is ignored in the USAT Portal.
 * 
 * @author aeast
 * 
 */
public class MenuOptions {

	private String label = null;
	private String url = null;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "MenuOptions [label=" + label + ", url=" + url + "]";
	}

}
