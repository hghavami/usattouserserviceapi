package com.gannett.usat.userserviceapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.userserviceapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.changeUserPassword.ChangeUserPasswordResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the UserService API for vacation
 * stops/starts
 * 
 * @author hghavami
 * 
 */
public class ChangeUserPassword extends UserServiceAPIBase {

	/**
	 * Converts json returned from the UserService Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static ChangeUserPasswordResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		ChangeUserPasswordResponse response = null;

		Type getUserResponseType = new TypeToken<ChangeUserPasswordResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, getUserResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new ChangeUserPasswordResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public ChangeUserPasswordResponse createChangeUserPassword(String emailAddress, String newPassword) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + CHANGE_USER_PASSWORD_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (emailAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		}

		if (newPassword != null) {
			nameValuePairs.add(new BasicNameValuePair("newPassword", newPassword));
		}

		if (UserServiceAPIContext.getApiMarketId() != null) {
			nameValuePairs.add(new BasicNameValuePair("marketId", UserServiceAPIContext.getApiMarketId()));
		}

		if (UserServiceAPIContext.getApiExternalSecret() != null && !UserServiceAPIContext.getApiExternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("fireflyExternalSecret", UserServiceAPIContext.getApiExternalSecret()));			
		}
		
		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		ChangeUserPasswordResponse response = ChangeUserPassword.jsonToCreateUserResponse(responseJSON);

		return response;

	}
}
