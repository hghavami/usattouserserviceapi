package com.gannett.usat.userserviceapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.client.utils.URIBuilder;

import com.gannett.usat.userserviceapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.getUser.GetUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the UserService API for vacation
 * stops/starts
 * 
 * @author hghavami
 * 
 */
public class GetUser extends UserServiceAPIBase {

	/**
	 * Converts json returned from the UserService Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static GetUserResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		GetUserResponse response = null;

		Type getUserResponseType = new TypeToken<GetUserResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, getUserResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new GetUserResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public GetUserResponse getUsers(String emailAddress) throws Exception {

		String responseJSON = null;
		String fireflyExternalSecret = null;
		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort()).setPath(url.getPath() + GET_USER_PATH);
	
		if (UserServiceAPIContext.getApiExternalSecret() != null && !UserServiceAPIContext.getApiExternalSecret().trim().equals("")) {
			fireflyExternalSecret = UserServiceAPIContext.getApiExternalSecret();			
		}
		builder.addParameter("fireflyExternalSecret", fireflyExternalSecret);
		// build up URL
		java.net.URI uri = builder.build();

		StringBuilder uriString = new StringBuilder(uri.toString());
		uriString.append("&email=").append(URLEncoder.encode(emailAddress, "UTF-8"));
		responseJSON = this.makeAPIGetRequest(uriString.toString());

		GetUserResponse response = GetUser.jsonToCreateUserResponse(responseJSON);

		return response;

	}
}
