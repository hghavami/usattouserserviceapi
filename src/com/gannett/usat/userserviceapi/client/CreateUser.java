package com.gannett.usat.userserviceapi.client;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gannett.usat.userserviceapi.domainbeans.ErrorsDeserializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.users.UsersResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class is used to communicate with the UserService API for vacation stops/starts
 * 
 * @author hghavami
 * 
 */
public class CreateUser extends UserServiceAPIBase {

	/**
	 * Converts json returned from the UserService Subscription API to Objects
	 * 
	 * @param json
	 *            - A json string as returned from the API
	 * @return Collection of SubscriberAccount objects
	 */
	public static UsersResponse jsonToCreateUserResponse(String json) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Meta.class, new ErrorsDeserializer());
		Gson gson = gsonBuilder.create();

		UsersResponse response = null;

		Type createUserResponseType = new TypeToken<UsersResponse>() {
		}.getType();

		try {
			response = gson.fromJson(json, createUserResponseType);
		} catch (Exception e) {
			e.printStackTrace();
			if (response == null) {
				response = new UsersResponse();
			}
			response.setRawResponse(json);
		}

		return response;
	}

	public UsersResponse createUser(String firstName, String lastName, String emailAddress, String password) throws Exception {

		String responseJSON = null;

		URL url = new URL(this.getBaseAPIURL());

		URIBuilder builder = new URIBuilder();
		builder.setScheme(url.getProtocol()).setHost(url.getHost()).setPort(url.getPort())
				.setPath(url.getPath() + CREATE_USER_PATH);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		if (UserServiceAPIContext.getApiModifiedBy() != null && !UserServiceAPIContext.getApiModifiedBy().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("modifiedBy", UserServiceAPIContext.getApiModifiedBy()));
		}

		if (firstName != null) {
			nameValuePairs.add(new BasicNameValuePair("firstName", firstName));
		}

		if (lastName != null) {
			nameValuePairs.add(new BasicNameValuePair("lastName", lastName));
		}

		if (emailAddress != null) {
			nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		}

		if (password != null) {
			nameValuePairs.add(new BasicNameValuePair("password", password));
		}

		if (UserServiceAPIContext.getApiMarketId() != null) {
			nameValuePairs.add(new BasicNameValuePair("marketId", UserServiceAPIContext.getApiMarketId()));
		}

		if (UserServiceAPIContext.getApiExternalSecret() != null && !UserServiceAPIContext.getApiExternalSecret().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("fireflyExternalSecret", UserServiceAPIContext.getApiExternalSecret()));
		}

		if (UserServiceAPIContext.getApiCredentialType() != null && !UserServiceAPIContext.getApiCredentialType().trim().equals("")) {
			nameValuePairs.add(new BasicNameValuePair("credentialType", UserServiceAPIContext.getApiCredentialType()));
		}

		// build up URL
		java.net.URI uri = builder.build();

		String uriString = uri.toString();

		responseJSON = this.makeAPIPostRequest(uriString, nameValuePairs);

		UsersResponse response = CreateUser.jsonToCreateUserResponse(responseJSON);

		return response;

	}
}
