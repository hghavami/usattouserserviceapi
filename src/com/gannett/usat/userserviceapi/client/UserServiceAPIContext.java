package com.gannett.usat.userserviceapi.client;

public class UserServiceAPIContext {

	protected static String endPointURL = null;
	protected static String endPointUserServiceURL = null;
	protected static String apiMarketId = "9999";
	public static boolean debugMode = false;
	public static String apiUserPwd = "";
	public static String apiUserID = "";
	public static String apiExternalSecret = null;
	public static String apiInternalSecret = null;
	public static String apiModifiedBy = "USA TODAY";
	public static String apiCredentialType = "Standard";
	public static String apiOnSuccessRedirectUrl = "";

	
	public static String getApiModifiedBy() {
		return apiModifiedBy;
	}

	public static void setApiModifiedBy(String apiModifiedBy) {
		UserServiceAPIContext.apiModifiedBy = apiModifiedBy;
	}

	public static String getEndPointURL() {
		return endPointURL;
	}

	public static void setEndPointURL(String endPointURL) {
		UserServiceAPIContext.endPointURL = endPointURL;
	}

	public static String getApiMarketId() {
		return apiMarketId;
	}

	public static void setApiMarketId(String apiMarketId) {
		UserServiceAPIContext.apiMarketId = apiMarketId;
	}

	public static boolean isDebugMode() {
		return debugMode;
	}

	public static void setDebugMode(boolean debugMode) {
		UserServiceAPIContext.debugMode = debugMode;
	}

	public static String getEndPointUserServiceURL() {
		return endPointUserServiceURL;
	}

	public static void setEndPointUserServiceURL(String endPointUserServiceURL) {
		UserServiceAPIContext.endPointUserServiceURL = endPointUserServiceURL;
	}

	public static String getApiUserPwd() {
		return apiUserPwd;
	}

	public static void setApiUserPwd(String apiUserPwd) {
		UserServiceAPIContext.apiUserPwd = apiUserPwd;
	}

	public static String getApiUserID() {
		return apiUserID;
	}

	public static void setApiUserID(String apiUserID) {
		UserServiceAPIContext.apiUserID = apiUserID;
	}

	public static String getApiExternalSecret() {
		return apiExternalSecret;
	}

	public static void setApiExternalSecret(String apiExternalSecret) {
		UserServiceAPIContext.apiExternalSecret = apiExternalSecret;
	}

	public static String getApiInternalSecret() {
		return apiInternalSecret;
	}

	public static void setApiInternalSecret(String apiInternalSecret) {
		UserServiceAPIContext.apiInternalSecret = apiInternalSecret;
	}

	public static String getApiCredentialType() {
		return apiCredentialType;
	}

	public static void setApiCredentialType(String apiCredentialType) {
		UserServiceAPIContext.apiCredentialType = apiCredentialType;
	}

	public static String getApiOnSuccessRedirectUrl() {
		return apiOnSuccessRedirectUrl;
	}

	public static void setApiOnSuccessRedirectUrl(String apiOnSuccessRedirectUrl) {
		UserServiceAPIContext.apiOnSuccessRedirectUrl = apiOnSuccessRedirectUrl;
	}
}