package com.gannett.usat.userserviceapi.unittest;

import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;
import com.gannett.usat.userserviceapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.getUser.GetUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GetUserTest extends BaseTest {

	public static void main(String[] args) {

		GetUserTest test = new GetUserTest();

		test.setUpAPIContext();

		test.testGetGetUser();
	}

	protected void testGetGetUser() {

		try {
			com.gannett.usat.userserviceapi.client.GetUser cUser = new com.gannett.usat.userserviceapi.client.GetUser();

			UserServiceAPIContext.setApiMarketId("USAT");
			UserServiceAPIContext.setApiInternalSecret("HOLLY");

			GetUserResponse response = cUser.getUsers("hghavami@usatoday.com");

			String json = response.getRawResponse();

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println("Raw JSON: " + json);
			System.out.println("Any Errors: " + response.getErrorMessages().toString());
			System.out.println("Any Error Code: " + response.getErrorCode());
			System.out.println("Any Error Message: " + response.getMessage());
			System.out.println("User ID: " + response.getFireflyUserId().toString());				
			System.out.println("back to json....");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

