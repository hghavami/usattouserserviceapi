package com.gannett.usat.userserviceapi.unittest;

import java.io.Console;
import java.io.InputStreamReader;

import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;

public class BaseTest {

	public String getInput(String prompt) {
		String input = null;

		if (prompt == null || prompt.length() == 0) {
			prompt = "Enter input [q to quit]:";
		}

		Console c = System.console();
		if (c == null) {

			InputStreamReader cin = new InputStreamReader(System.in);

			System.out.println(prompt);

			try {
				char[] cbuf = new char[256];
				cin.read(cbuf);

				String tempinput = new String(cbuf);
				if (tempinput != null && tempinput.trim().length() > 0) {
					input = tempinput.trim();
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else {
			input = c.readLine(prompt);
		}

		if (input == null || input.trim().length() == 0) {
			;
		} else if (input.trim().equalsIgnoreCase("q")) {
			System.out.println("Exiting Normally.");
			System.exit(0);
		} else {
			input = input.trim();
		}

		return input;
	}

	public void setUpAPIContext() {
		UserServiceAPIContext.setEndPointURL("http://aimepublic.qa.gmti.gbahn.net/User/v4");
		UserServiceAPIContext.setApiUserID("");
		UserServiceAPIContext.setApiUserPwd("");
		UserServiceAPIContext.setApiMarketId("USAT");
		UserServiceAPIContext.setDebugMode(true);
		UserServiceAPIContext.setApiModifiedBy("USA TODAY");
		UserServiceAPIContext.setApiExternalSecret("e37a3157650d3fea025b9afd8c3cd11d");
		UserServiceAPIContext.setApiCredentialType("Standard");
	}
}
