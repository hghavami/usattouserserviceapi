package com.gannett.usat.userserviceapi.unittest;

import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;
import com.gannett.usat.userserviceapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.changeUserPassword.ChangeUserPasswordResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ChangeUserPasswordTest extends BaseTest {

	public static void main(String[] args) {

		ChangeUserPasswordTest test = new ChangeUserPasswordTest();

		test.setUpAPIContext();

		test.testChangeUserPassword();
	}

	protected void testChangeUserPassword() {

		try {
			com.gannett.usat.userserviceapi.client.ChangeUserPassword cUser = new com.gannett.usat.userserviceapi.client.ChangeUserPassword();

			UserServiceAPIContext.setApiMarketId("USAT");

			ChangeUserPasswordResponse response = cUser.createChangeUserPassword("hghavami@usatoday.com", "password");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println("Raw JSON: " + json);
			System.out.println("Any Errors: " + response.getErrorMessages().toString());
			System.out.println("Any Error Code: " + response.getErrorCode());
			System.out.println("Any Error Message: " + response.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

