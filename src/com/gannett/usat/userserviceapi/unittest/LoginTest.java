package com.gannett.usat.userserviceapi.unittest;

import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;
import com.gannett.usat.userserviceapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.login.LoginResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LoginTest extends BaseTest {

	public static void main(String[] args) {

		LoginTest test = new LoginTest();

		test.setUpAPIContext();

		test.testGetLogin();
	}

	protected void testGetLogin() {

		try {
			com.gannett.usat.userserviceapi.client.Login cUser = new com.gannett.usat.userserviceapi.client.Login();

			UserServiceAPIContext.setApiMarketId("USAT");
			UserServiceAPIContext.setApiInternalSecret("HOLLY");

			LoginResponse response = cUser.getLogin("hghavami@usatoday.com", "password");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();
			
			System.out.println(gson2.toJson(response));
			System.out.println("Raw JSON: " + json);
			System.out.println("Any Errors: " + response.getErrorMessages().toString());
			System.out.println("Any Error Code: " + response.getErrorCode());
			System.out.println("Any Error Message: " + response.getMessage());
			if (response.getUserId() != null  && !response.getUserId().trim().equals("")) {
				System.out.println("User ID: " + response.getUserId().toString());				
			}
			System.out.println("back to json....");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

