package com.gannett.usat.userserviceapi.unittest;

import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;
import com.gannett.usat.userserviceapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.users.UsersResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CreateUserTest extends BaseTest {

	public static void main(String[] args) {

		CreateUserTest test = new CreateUserTest();

		test.setUpAPIContext();

		test.testCreateCreateUser();
	}

	protected void testCreateCreateUser() {

		try {
			com.gannett.usat.userserviceapi.client.CreateUser cUser = new com.gannett.usat.userserviceapi.client.CreateUser();

			UserServiceAPIContext.setApiMarketId("USAT");

			UsersResponse response = cUser.createUser("Mark76", "Jacobs76", "hghavami76@gannett.com", "password76");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println("Raw JSON: " + json);
			System.out.println("Any Errors: " + response.getErrorMessages().toString());
			System.out.println("Any Error Code: " + response.getErrorCode());
			System.out.println("Any Error Message: " + response.getMessage());
			System.out.println("Firefly User ID: " + response.getFireflyUserId().toString());

			System.out.println("back to json....");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

