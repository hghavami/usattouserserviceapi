package com.gannett.usat.userserviceapi.unittest;

import com.gannett.usat.userserviceapi.client.UserServiceAPIContext;
import com.gannett.usat.userserviceapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.userserviceapi.domainbeans.Meta;
import com.gannett.usat.userserviceapi.domainbeans.changePassword.ChangePasswordResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ChangePasswordTest extends BaseTest {

	public static void main(String[] args) {

		ChangePasswordTest test = new ChangePasswordTest();

		test.setUpAPIContext();

		test.testChangePassword();
	}

	protected void testChangePassword() {

		try {
			com.gannett.usat.userserviceapi.client.ChangePassword cUser = new com.gannett.usat.userserviceapi.client.ChangePassword();

			UserServiceAPIContext.setApiMarketId("USAT");
			UserServiceAPIContext.setApiInternalSecret("HOLLY");

			ChangePasswordResponse response = cUser.createChangePassword("143771443", "password1", "password");

			String json = response.getRawResponse();

			System.out.println("Raw JSON: " + json);

			System.out.println("back to json....");

			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Meta.class, new ErrorsSerializer());

			Gson gson2 = builder.create();

			System.out.println(gson2.toJson(response));
			System.out.println(response.getErrorMessages().toString());
			System.out.println(response.getRawResponse().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

